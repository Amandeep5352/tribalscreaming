package activity4_5;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestTribalScreaming {

	
	// first test case
	@Test
	public void testOnePersonscream() {
		TribalScreaming sn = new TribalScreaming();
		String result = sn.scream("Peter");
		assertEquals("Peter is amazing",result);
	
	
	}

	//second test case
	@Test
	public void testnobodyListening() {
		TribalScreaming sn = new TribalScreaming();
		String result = sn.scream(null);
		assertEquals("You are amazing",result);
	
	
	}
	
	//third test case
	@Test
	public void testPersonShouting() {
		TribalScreaming sn = new TribalScreaming();
		String result = sn.scream("PETER");
		assertEquals("PETER IS AMAZING",result);
	
	
	}
	
	//fourth test case
	@Test
	public void testTwoPerson() {
		TribalScreaming sn = new TribalScreaming();
		String[] sname = new String[] { "peter", "jigesha"};
		String result = sn.screamName(sname);
		assertEquals("peter and jigesha are amazing",result);
	
	
	}
	
	//fifth test case
	@Test
	public void testTwoOrMorePerson() {
		TribalScreaming sn = new TribalScreaming();
		String[] sname = new String[] { "Peter", "Jigesha","Albert"};
		String result = sn.screamName(sname);
		assertEquals("Peter, Jigesha and Albert are amazing",result);
	
	
	}
	//sixth test case
	
		@Test
		public void testuppercase() {
			TribalScreaming sn = new TribalScreaming();
			String[] sname = new String[] { "Peter", "Jigesha","Albert","MARCOS"};
			String result = sn.screamName(sname);
			assertEquals("Peter, Jigesha and Albert are amazing. MARCOS ALSO!",result);
		
		
		}
	
}
